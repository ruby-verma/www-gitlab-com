module FeatureHelpers
  def missing_feature(feature)
    !(feature.gitlab_core || feature.gitlab_starter || feature.gitlab_premium || feature.gitlab_ultimate)
  end
end
