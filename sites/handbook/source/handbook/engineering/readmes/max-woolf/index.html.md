---
layout: markdown_page
title: "Max Woolf's README"
job: "Senior Backend Engineer"
---

## Max Woolf's README

**Max Woolf - Senior Backend Engineer, Manage:Compliance**

This page is intended to help others understand what it might be like to work with me, especially people who haven’t worked with me before. 

It’s also a well-intentioned effort at building some trust by being intentionally vulnerable, and to share my ideas of a good working relationship to reduce the anxiety of people who might be on my team.

Please feel free to contribute to this page by opening a merge request. 

## Related pages

* [LinkedIn](https://www.linkedin.com/in/max-woolf-488bb534/)
* [Personal CV & Blog](https://max.woolf.io)

## About me

* Professionally
  * I've been a software engineer since 2011, focusing almost exclusively on Ruby and Rails.
  * I've worked mostly at early-stage startups and agencies so GitLab is the largest company I have worked for.
  * I joined GitLab on April 20th, 2020.
  * I am AWS SysOps certified.

* Personally
  * I live in Birmingham, England with my wife and dog.
  * I attended University of Sussex in Brighton and studied [Music Informatics](https://en.wikipedia.org/wiki/Music_informatics). (Creativity in SWE is really important to me.)
  * I used to be a half-decent pianist.
  * I love walking, baking and playing an active part in my local community.

The values of **Iteration** and **Diversity, Inclusion and Belonging** are very important to me and inform my working practices.

Specifically the sub-values of having a "**low level of shame**" and being supportive and accepting of **neurodiversity**.

## What I assume about others

* Everyone has something to teach me. Sometimes, I have something to teach them.
* [Everyone is intelligent and well-meaning.](https://github.com/thoughtbot/guides/tree/master/code-review#everyone)

## Communicating with me

* I'm generally working 8am - 5pm UK time (+/- 2 hours).
* I'm always available for coffee chats. One of the benefits of working at GitLab is meeting people from across the world and I want to!
* In general, I prefer Slack over Email for anything that doesn't belong in an Issue or Merge Request.
* Don't hesitate to ask me anything. If I can help, I will! 

## Things I'm trying to improve

* I'm not great at reading between the lines as a way of accepting criticism or constructive feedback. **If something is wrong, please say so.** I will thank you for it!
* I can sometimes act too much like a magpie; paying too much attention to the "new and shiny" things and not enough to the "older but more reliable" things.
