---
layout: markdown_page
title: "GitLab adds Emerasoft to Global Partnership Program"
---

**San Francisco (Jul 31st, 2017)** – Today, GitLab announced a partnership with Emerasoft Srl., an Italian company providing software development products, consulting and training services. Emerasoft specializes in solving complex technical challenges, especially in the areas of ALM, DevOps, Software Testing, Security, IoT, and Business Process Intelligence. Emerasoft will now sell and support GitLab products as part of their portfolio. The team will provide support to customers during their onboarding process, helping them not only take advantage of GitLab’s repository management, but also the product’s issue management, continuous integration, and continuous delivery capabilities. 

“GitLab can help our customers solve the problem of having to manage different DevOps tools and provide them with a single software where to integrate the different activities,”  says Riccardo Bernasconi, Emerasoft Sales Manager. 

“Emerasoft has a wealth of experience working in the software development products and services space. With their experience in ALM and DevOps, Emerasoft is advantageously positioned to support Italian-based customers in their efforts to adopt GitLab for their full software development lifecycle,” says Michael Alessio, GitLab’s Director of Global Alliances.

Emerasoft Srl. joins a growing community of GitLab partners, around the world. For a complete list of GitLab resellers, please visit [/resellers/](/resellers/).

**About Emerasoft Srl.** 

Founded in 2005, Emerasoft has strong expertise in a variety of different areas of the IT business, ranging from ALM and DevOps to Software Testing and IoT. The team's expertise is reflected in the their ability to deal with heterogeneous and complex problems in professional and qualified way. Emerasoft works with customers to select state-of-the-art technologies, navigate new frontiers of application development, and build a path of excellence and quality.

For more information about Emerasoft Srl., please visit:
- [Emerasoft website](https://www.emerasoft.com) or write to `gitlab@emerasoft.com`
- [GitLab-specific webpage](http://www.emerasoft.com/devops-servizi-consulenza-strumenti/gitlab/)  
- [Emerasoft Facebook page](https://www.facebook.com/emerasoft)
- [Emerasoft Twitter page](https://twitter.com/emerasoft)
- [Emerasoft LinkedIn page](https://www.linkedin.com/company-beta/1203309/)

**About GitLab**
GitLab is a DevOps platform built from the ground up as a single application for all stages of the DevOps lifecycle enabling Product, Development, QA, Security, and Operations teams to work concurrently on the same project. GitLab provides a single data store, one user interface, and one permission model across the DevOps lifecycle. This allows teams to significantly reduce cycle time through more efficient collaboration and enhanced focus. Built on Open Source, GitLab leverages the community contributions of thousands of developers and millions of users to continuously deliver new DevOps innovations. More than 100,000 organizations from startups to global enterprises, including Ticketmaster, Jaguar Land Rover, NASDAQ, Dish Network, and Comcast trust GitLab to deliver great software faster. GitLab is the world's largest all-remote company, with more than 1,200 team members in more than 65 countries and regions.


#### Media Contact
Natasha Woods
<br> 
GitLab
<br> 
press@gitlab.com
