---
layout: markdown_page
title: "CircleCI"
---
<!-- This is the template for defining sections which will be included in a tool comparison page. This .md file is included in the top of the page and a table of feature comparisons is added directly below it. This template provides the sections which can be included, and the order to include them. If a section has no content yet then leave it out. Leave this note in tact so that others can see where new sections should be added.

## Summary
   - minimal requirement <-- comment. delete this line
## Strengths
## Weaknesses
## Who buys and why
## Comments/Anecdotes
   - possible customer issues with product  <-- comment. delete this line
   - sample benefits and success stories  <-- comment. delete this line
   - date, source, insight  <-- comment. delete this line
## Resources
   - links to communities, etc  <-- comment. delete this line
   - bulleted list  <-- comment. delete this line
## FAQs
 - about the product  <-- comment. delete this line
## Integrations
## Pricing
   - summary, links to tool website  <-- comment. delete this line
### Value/ROI
   - link to ROI calc?  <-- comment. delete this line
## Questions to ask
   - positioning questions, traps, etc.  <-- comment. delete this line
## Comparison
   - features comparison table will follow this <-- comment. delete this line

<!------------------Begin page additions below this line ------------------ -->

## On this page
{:.no_toc}

- TOC
{:toc}

## GitLab-CircleCI Comparison Infographic
  This summary infographic compares CircleCI and GitLab across several DevOps Stages and Categories.

![GitLab CircleCI Comparison Chart](/devops-tools/circleci/GitLab_CircleCI.jpg)

## Summary

Founded in 2011 and headquartered in San Francisco, Ca., CircleCI  provides a service that automates the Continuous Integration stage of the Software Development Life Cycle (SDLC).  Their CI service offering can be hosted in the cloud or on a private server.  CI jobs are built within four different environments: a Docker image, a Linux VM, Windows VM, or a MacOS VM.  They demonstrate their support of the Open Source Community by providing organizations with free credits for Open Source builds.

**CircleCI Orbs**

CircleCI can provide automated services for other stages of the Software Development Life Cycle (SDLC) using third party plug-ins that they call “Orbs”.  They define Orbs as reusable/sharable packages of YAML configurations that condenses repeated pieces of configs into a single line of code. In other words, think of Orbs as a pointer that is included in the YAML configuration file that activates a piece of code during the build process that performs a function.  The Orbs are housed in an open source code library.

## Strengths

* YAML File:  CI build jobs are configured in a YAML file
* Security Accreditations: CircleCI is FedRamp authorized and SOC 2 compliant
* Forrester Wave: CircleCI was recognized as a Leader in The Forrester Wave™: Cloud-Native Continuous Integration Tools, Q3 2019: speed, scale, security, and compliance.
* Catering to Large Enterprise:
   - CircleCI provides a large number of preconfigured environments, which is highly favored by enterprises
   - Despite a team size of just over 300 employees, CircleCI has a Customer Success Team and Enterprise support packages, also favored by enterprises
* iOS application testing on macOS: CircleCI offers support for building and testing iOS projects in macOS virtual machines (available on CircleCI Cloud only, not currently available on self-hosted installations).  
   - GitLab is actively working on integrating this functionality, more details can be found [here](https://gitlab.com/gitlab-com/gl-infra/infrastructure/-/issues/5720)


## Gaps

* Free Plan Trap: Their free plan offers a generous amount of free credits, 2,500/week.  However, these credits are used to pay for CI run times across medium sized Linux and Windows machines only; and are used for Orb usage, Workspaces and Dependency Caching. 
* No Single Integrated DevOps Application: CircleCI is a tool that automates the Continuous Integration stage of the Software Development Life Cycle.  To extend the functionality beyond CI, integration with third party plugins is required.  Plugins are expensive to maintain, secure, and upgrade. In contrast, GitLab is [open core](https://about.gitlab.com/blog/2016/07/20/gitlab-is-open-core-github-is-closed-source/) and anyone can contribute changes directly to the codebase, which once merged would be automatically tested and maintained with every change.
* Missing Enterprise Features: CircleCI lacks native support for key enterprise features such as Feature Flags, Kubernetes Support and Canary Deployments.
* Hybride CI: CircleCI lacks the ability to orchestrate a customers private server CI builds with their cloud hosted CI server

## Pricing Plans

* Free ($0)
   - 2,500 free credits/week
   - Run 1 job at a time
   - Build on Linux or Window (no macOS support)
   - Does not support flexible payment option (credit card or invoice)
   - Support Options:
      - Community
* Performance (starting at $30/month)
   - $15/month for the first 3 users and then $15/month for each added user
   - Starts at 25,000 credits for $15
   - Does not support flexible payment option (credit card or invoice)
   - Support Options:
      - Community
      - Support Portal
      - Global Ticket Support
      - 8×5 SLAs available
      - Account Team: Customer Success Manager 
* Custom
   - Plan customized for the customer
   - Support Options:
      - Community
      - Support Portal
      - Global Ticket Support
      - 24×5 and 24×7 SLAs available
      - Account Team: Customer Success Manager, Customer Success Engineer, Implementation Manager

## CircleCI to GitLab Migration

This [Issue](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/32487) provides guidance on migrating CircleCI to GitLab.

## Comparison
